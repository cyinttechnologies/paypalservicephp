<?php
/*PaypalService.php
Symfony Service to connect to Paypal
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



namespace CYINT\ComponentsPHP\Services;
use \Braintree_Gateway;
use Doctrine\Bundle\DoctrineBundle\Registry;

class PaypalService
{
    protected $Doctrine = null;
    protected $Gateway = null;
    protected $currency = 'USD';

    function __construct($Doctrine)
    {  
        $this->setDoctrine($Doctrine);
        $Repository = $Doctrine->getRepository('CYINTSettingsBundle:Setting');
        $paypal = $Repository->findByNamespace('paypal');
        $checkout = $Repository->findOneBy(['settingKey'=>'checkout']);
        $this->setCurrency($checkout['currency']);
        $this->setAccessToken($paypal['access_token']);
        $this->setConfiguration();
    }

    function setConfiguration()
    {
        $this->setGateway(new Braintree_Gateway([
            'accessToken'=>$this->getAccessToken()
        ]));
    }

    public function generateClientToken()
    {
        return $this->getGateway()->clientToken()->generate();
    }

    public function setGateway(Braintree_Gateway $Gateway)
    {
        $this->Gateway = $Gateway;
    }

    public function getGateway()
    {
        return $this->Gateway;
    }

    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;     
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function setDoctrine(Registry $Doctrine)
    {
        $this->Doctrine = $Doctrine;
    }

    public function getDoctrine()
    {
        return $this->Doctrine;
    }

    public function initiateTransaction($email, $token, $amount) 
    {
        try 
        {

            return ['result' => $this->getGateway()->transaction()->sale([
                'amount' => $amount,
                'merchantAccountId'=>$this->getCurrency(),
                'paymentMethodNonce'=>$token             
            ])];
        } 
        catch(\Exception $e) 
        {
            return ['result'=>false, 'error'=>$e];
        }
    }
}



?>
